from PropertyManagement.model.DBModel import AddressDB, ComputerPersonDB, PersonDB
from PropertyManagement.model.UserProfileModel import Address, Person, ComputerPerson

__author__ = 'davegermiquet'


class AddressSaver:
    address = Address()

    def __init__(self, address_input, db):
        self.address = address_input
        self.db = db
    def save(self):
        address_db = AddressDB()
        address_db.street = self.address.street_name
        address_db.suite = self.address.suite
        address_db.house_no = self.address.street_number
        address_db.postal_code = self.address.postal_code
        address_db.city = self.address.city
        address_db.province = self.address.province
        address_db.country = self.address.country
        self.db.session.add(address_db)
        self.db.session.commit()

        return address_db.address_id


class PersonSaver:
    person = Person()

    def __init__(self, person_input, db_input):
        self.person = person_input
        self.db = db_input
    def save(self):
        person_db = PersonDB()
        person_db.first_name = self.person.first_name
        person_db.last_name = self.person.last_name
        person_db.middle_name = self.person.middle_name
        person_db.phone = self.person.phone
        self.db.session.add(person_db)
        self.db.session.commit()
        return person_db.person_id


class ComputerPersonSaver:
    computer_person = ComputerPerson()
    person = Person()

    def __init__(self, computer_person_input, person_input, db_input):
        self.computer_person = computer_person_input
        self.person = person_input
        self.db = db_input

    def save(self):
        if self.person is None:
            raise Exception("Person is not assigned")
        if self.computer_person is None:
            raise Exception("Computer Person is not assigned")
        person_inserter = PersonSaver(self.person, self.db)
        person_id = person_inserter.save()
        computer_person_db = ComputerPersonDB()
        computer_person_db.username = self.computer_person.username
        computer_person_db.password = self.computer_person.password
        computer_person_db.person_id = person_id
        self.db.session.add(computer_person_db)
        self.db.session.commit()
        return computer_person_db.person_id









