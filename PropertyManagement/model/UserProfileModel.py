__author__ = 'davegermiquet'


class Address:
    id = None
    street_number = None
    street_name = None
    city = None
    country = None
    suite = None
    postal_code = None
    province = None


class Person:
    id = None
    first_name = None
    middle_name = None
    last_name = None
    email_address = None
    phone = None
    date_time = None
    address = None


class ComputerPerson:
    id = None
    username = None
    password = None
    rest_password = None
    person = None
    access_type = None


class LandLord:
    landlord_id = None
    person_id = None
    time_stamp_created = None


class Company:
    name = None
    address = None
    phone_number = None
    email_address = None
    receipt_height = None
    receipt_width = None
    receipt_background = None


class Tenants:
    tenant_id = None
    person = None
    deposit = None
    notes = None
    status = None