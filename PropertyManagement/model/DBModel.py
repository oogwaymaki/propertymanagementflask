from sqlalchemy.dialects.mysql import INTEGER as Integer

from PropertyManagement import db


__author__ = 'davegermiquet'


class CompanyDB(db.Model):
    __tablename__ = 'company'
    name = db.Column(db.String(20), primary_key=True)
    phone_number = db.Column(db.String(20), nullable=False)
    email_address = db.Column(db.String(30), nullable=False)
    receipt_height = db.Column(Integer(unsigned=True))
    receipt_width = db.Column(Integer(unsigned=True))
    receipt_background = db.Column(db.BLOB)


class AddressDB(db.Model):
    __tablename__ = 'address'
    address_id = db.Column(Integer(unsigned=True), primary_key=True, name='address_id')
    suite = db.Column(Integer, nullable=True)
    house_no = db.Column(db.String(10), nullable=False, name='house_no')
    street = db.Column(db.String(30), nullable=False, name='street')
    province = db.Column(db.String(30), nullable=False, name='province')
    postal_code = db.Column(db.String(6), nullable=False, name='postal_code')
    country = db.Column(db.String(2), nullable=False, name='country')
    city = db.Column(db.String(30), nullable=False, name='city')


class Addressperson(db.Model):
    __tablename__ = 'addressperson'
    addressperson_id = db.Column(Integer(unsigned=True), primary_key=True)
    address_id = db.Column(Integer(unsigned=True), db.ForeignKey('address.address_id'))
    person_id = db.Column(Integer(unsigned=True), db.ForeignKey('person.person_id'))


class PersonDB(db.Model):
    __tablename__ = 'person'
    person_id = db.Column(Integer(unsigned=True), primary_key=True)
    first_name = db.Column(db.String(30), nullable=False)
    middle_name = db.Column(db.String(30), nullable=True)
    last_name = db.Column(db.String(30), nullable=False)
    email_address = db.Column(db.String(255), nullable=True)
    phone = db.Column(db.String(30), nullable=True)
    date_time = db.Column(db.TIMESTAMP, nullable=False)


class ComputerPersonDB(db.Model):
    __tablename__ = 'computer_person'
    username_id = db.Column(Integer(unsigned=True), primary_key=True)
    username = db.Column(db.String(10), nullable=False)
    password = db.Column(db.String(30), nullable=False)
    person_id = db.Column(Integer(unsigned=True), db.ForeignKey('person.person_id'))

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.username_id)

    def __repr__(self):
        return '<User %r>' % (self.username)


class Landlords(db.Model):
    __tablename__ = 'landlords'
    landlord_id = db.Column(Integer(unsigned=True), primary_key=True)
    person_id = db.Column(Integer(unsigned=True), db.ForeignKey('person.person_id'))
    date_time = db.Column(db.TIMESTAMP, nullable=False)


class Tenants(db.Model):
    __tablename__ = 'tenants'
    tenant_id = db.Column(Integer(unsigned=True), primary_key=True)
    person_id = db.Column(Integer(unsigned=True), db.ForeignKey('person.person_id'))
    deposit_amount = db.Column(db.Numeric(12, 2), nullable=False)
    notes = db.Column(db.Text)
    status = db.Column(db.Enum('current', 'former'))


class Buildings(db.Model):
    __tablename__ = 'buildings'
    building_id = db.Column(Integer(unsigned=True), primary_key=True)
    address_id = db.Column(Integer(unsigned=True), db.ForeignKey('address.address_id'))
    landlord_id = db.Column(Integer(unsigned=True), db.ForeignKey('landlords.landlord_id'))
    commission = db.Column(Integer(unsigned=True), nullable=False)
    building_name = db.Column(db.String(20), nullable=False)
    notes = db.Column(db.Text)
    landlord_permission = db.Column(db.Enum('yes', 'no'))


class Apartments(db.Model):
    __tablename__ = 'apartment'
    apartment_id = db.Column(Integer(unsigned=True), primary_key=True)
    monthly_rent = db.Column(db.Numeric(12, 2), nullable=False)
    building_id = db.Column(Integer(unsigned=True), db.ForeignKey('buildings.building_id'))
    notes = db.Column(db.Text)
    apartment_label = db.Column(db.String(12), nullable=True)


class DepositRefunds(db.Model):
    __tablename__ = 'deposit_refunds'
    deposit_refund_id = db.Column(Integer(unsigned=True), primary_key=True)
    time = db.Column(db.TIMESTAMP, nullable=False)
    amount = db.Column(db.Numeric(10, 2), nullable=False)
    tenants_id = db.Column(Integer(unsigned=True), db.ForeignKey('tenants.tenant_id'))
    form = db.Column(db.Enum('cheque', 'cash'))


class RentPayment(db.Model):
    __tablename__ = 'rent_payment'
    receipt_id = db.Column(Integer(unsigned=True), primary_key=True)
    time = db.Column(db.TIMESTAMP, nullable=False)
    amount = db.Column(db.Numeric(10, 2), nullable=False)
    tenants_id = db.Column(Integer(unsigned=True), db.ForeignKey('tenants.tenant_id'))
    apartment_id = db.Column(Integer(unsigned=True), db.ForeignKey('apartment.apartment_id'))
    form = db.Column(db.Enum('cheque', 'cash'))
    receipt_type = db.Column(db.Enum('print', 'email'))
    purpose = db.Column(db.Enum('deposit', 'rent', 'fines', 'others'))


class PersonAddressTime(db.Model):
    __tablename__ = 'person_address_time'
    personaddress_time = db.Column(Integer(unsigned=True), primary_key=True)
    address_id = db.Column(Integer(unsigned=True), db.ForeignKey('address.address_id'))
    person_id = db.Column(Integer(unsigned=True), db.ForeignKey('person.person_id'))
    date_from = db.Column(db.TIMESTAMP, nullable=False)
    date_to = db.Column(db.TIMESTAMP, nullable=False)