import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


__author__ = 'davegermiquet'
tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root@localhost/jamar_db'
app.config['CSRF_ENABLED'] = True
app.config['SECRET_KEY'] = "ThisisRenchiesDasiaSecretKey"
db = SQLAlchemy(app)

from model import DBModel
from authentication.Auth import login_manager

db.create_all()

login_manager.init_app(app)

