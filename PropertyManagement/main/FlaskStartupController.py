from flask_login import logout_user, login_required, login_user
from flask import request, render_template, flash

from PropertyManagement import app, db
from PropertyManagement.Forms.Forms import RegistrationForm, LoginForm
from PropertyManagement.apiendpoint.RootEndPoint import RootEndPoint
from PropertyManagement.db.DatabaseSaver import ComputerPersonSaver
from PropertyManagement.model.DBModel import ComputerPersonDB
from PropertyManagement.model.UserProfileModel import Person, ComputerPerson


class FlaskStartupController:
    @app.route("/logout")
    @login_required
    def logout():
        logout_user()
        return "Logged out"

    @app.route('/public/login', methods=['POST', 'GET'], endpoint="login")
    def login():
        form = LoginForm(request.form)
        if request.method == 'POST':
            user = ComputerPersonDB.query.filter_by(username=form.username.data).first()
            if not (user is None):
                if not (user.password is None or form.password.data is None):
                    if user.password == form.password.data:
                        login_user(user)
                        return "You were able to login"

        return render_template('login.html', form=form)

    @app.route('/register', methods=['POST', 'GET'])
    #@login_required
    def register():
        form = RegistrationForm(request.form)

        if request.method == 'POST':
            person = Person()
            person.email_address = form.emailaddress.data
            person.first_name = form.firstname.data
            person.last_name = form.lastname.data
            person.middle_name = form.middlename.data
            person.phone = form.phonenumber.data
            user = ComputerPerson()
            user.username = form.username.data
            user.password = form.password.data
            computer_person_saver = ComputerPersonSaver(user, person, db)
            computer_person_saver.save()



            flash('Thanks for registering')
            return render_template('login.html', form=LoginForm(request.form))
        else:
            return render_template('register.html', form=form)


    def setupApi(self):
        pass

    def __init__(self):
        pass

    def register_api(self,view, endpoint, url, pk='id', pk_type='int'):
        view_func = view.as_view(endpoint)
        app.add_url_rule(url, defaults={pk: None},
                         view_func=view_func, methods=['GET',])
        app.add_url_rule(url, view_func=view_func, methods=['POST', ])
        app.add_url_rule('%s<%s:%s>' % (url, pk_type, pk), view_func=view_func,
                         methods=['GET', 'PUT', 'DELETE'])

    def run(self):
        app.debug = True
      #  self.register_api(RootEndPoint,"rootendpoint","/")
        app.add_url_rule('/', view_func=RootEndPoint.as_view('rootendpoint'))
        app.secret_key = '@#$@##lkflkdjfgklnvm,ncv$@#@#@#23kfdjklfj#$@#eds'
        app.run()



