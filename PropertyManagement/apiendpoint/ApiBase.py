from functools import wraps

from flask.views import MethodView
from flask_login import current_user

from PropertyManagement import login_manager


__author__ = 'davegermiquet'


def user_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        if not current_user.is_authenticated():
            return login_manager.unauthorized()
        return f(*args, **kwargs)

    return decorator


class ApiBase(MethodView):
    decorators = [user_required]

    def get(self):
        pass

    def post(self):
        pass

    def delete(self, id):
        pass

    def put(self, id):
        pass

