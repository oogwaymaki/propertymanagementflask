from flask import request
from flask_login import LoginManager
from werkzeug.utils import redirect

from PropertyManagement.model.DBModel import ComputerPersonDB


__author__ = 'davegermiquet'

login_manager = LoginManager()

login_manager.login_view = "login"


@login_manager.user_loader
def load_user(user_id):
    return ComputerPersonDB.query.get(user_id)


@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect('/login?' + request.path)

