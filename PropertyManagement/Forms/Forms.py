__author__ = 'davegermiquet'
from wtforms import Form, TextField, PasswordField, validators


class LoginForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=25)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')


class RegistrationForm(Form):
    username = TextField('username', [validators.Length(min=4, max=30)])
    firstname = TextField('firstname', [validators.Length(min=4, max=30)])
    lastname = TextField('lastname', [validators.Length(min=4, max=30)])
    middlename = TextField('middlename', [validators.Length(min=4, max=30)])
    phonenumber = TextField('phonenumber', [validators.Length(min=4, max=30)])
    emailaddress = TextField('emailaddress', [validators.Length(min=6, max=255)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')



